variable "vsphere_server" {
  default = "quinzy.kernarc.local"
}

variable "vsphere_user" {
  default = "administrator@vsphere.local"
}

variable "vsphere_pass" {} 
variable "root_pass" {}

variable "vsphere_datacenter" {
  default = "KERNARC"
}

variable "vsphere_network" {
  default = "INNER"
}

variable "vsphere_template" {
  default = "ubuntu2204_template"
}

variable "vsphere_cluster" {
  default = "Main"
}

variable "vsphere_datastore" {
  default = "SSD512"
}
