# Learn DevOps

Homework for DevOps course.

0. Use GitLab repo for config [+]

1. Deploy VM on VMware vSphere using Terraform Provider [+]

2. Deploy a (3-tier) app on VM using Ansible env [+]
